//
// Created by reimua on 26/11/18.
//

#pragma once

#include "Clock.hpp"
#include "ISprite.hpp"

#define PARALLAX_ID 6

class Parallax : public ISprite {
	Clock _clock;
	size_t _lastTotal;
	size_t _currentTotal;
public:
	Parallax(sf::Vector2f &pos);
	~Parallax();
	void update(ACore &) override;
	void onCollision(ACore &core, ISprite *other) override;
	void updateFromPacket(sf::Packet) override;
};

namespace NetworkConstructor {
	IEntity	*createParallaxFromPacket(ACore &core, sf::Packet packet);
};