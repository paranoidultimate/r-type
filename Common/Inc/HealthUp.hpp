//
// Created by kaww on 27/11/18.
//

#pragma once

#include "ISprite.hpp"

#define HEALTHUP_ID 13

class HealthUp : public ISprite {
	float _originalY;
	float _inc;
public:
	HealthUp(sf::Vector2f &pos);
	virtual ~HealthUp();
	void update(ACore &core) override;
	void onCollision(ACore &core, ISprite *ohter) override {};
};

namespace NetworkConstructor {
	IEntity *createHealthUpFromPacket(ACore &core, sf::Packet packet);
}