//
// Created by kaww on 27/09/18.
//

#pragma once

#include "IMonster.hpp"

#define VENERMONSTER_ID 23

class VenerMonster : public IMonster {
private:
    int _health;
public:
    VenerMonster(sf::Vector2f &pos, float scale = 1);
    ~VenerMonster();
    void updateMonster(ACore &Acore) override;
    void onCollision(ACore &, ISprite *) override;

    sf::Packet getEntityPacket(network::PACKET_TYPE) override;
    void updateFromPacket(sf::Packet packet) override;
};

namespace NetworkConstructor {
    IEntity *createVenerMonsterFromPacket(ACore &core, sf::Packet packet);
};