//
// Created by kaww on 27/09/18.
//

#pragma once

#include "IMonster.hpp"

#define SPEEDMONSTER_ID 24

class SpeedMonster : public IMonster {
private:
    int _health;
public:
    SpeedMonster(sf::Vector2f &pos, float scale = 1);
    ~SpeedMonster();
    void updateMonster(ACore &Acore) override;
    void onCollision(ACore &, ISprite *) override;

    sf::Packet getEntityPacket(network::PACKET_TYPE) override;
    void updateFromPacket(sf::Packet packet) override;
};

namespace NetworkConstructor {
    IEntity *createSpeedMonsterFromPacket(ACore &core, sf::Packet packet);
};