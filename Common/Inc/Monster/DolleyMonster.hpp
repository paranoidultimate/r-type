//
// Created by kaww on 27/09/18.
//

#pragma once

#include "IMonster.hpp"

#define DOLLEYMONSTER_ID 25

class DolleyMonster : public IMonster {
private:
    int _health;
public:
    DolleyMonster(sf::Vector2f &pos);
    ~DolleyMonster();
    void updateMonster(ACore &Acore) override;
    void onCollision(ACore &, ISprite *) override;

    sf::Packet getEntityPacket(network::PACKET_TYPE) override;
    void updateFromPacket(sf::Packet packet) override;
};

namespace NetworkConstructor {
    IEntity *createDolleyMonsterFromPacket(ACore &core, sf::Packet packet);
};