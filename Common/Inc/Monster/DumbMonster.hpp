//
// Created by kaww on 27/09/18.
//

#pragma once

#include "IMonster.hpp"

#define DUMBMONSTER_ID 22

class DumbMonster : public IMonster {
private:
    int _health;
public:
    DumbMonster(sf::Vector2f &pos, float scale = 1);
    ~DumbMonster();
    void updateMonster(ACore &Acore) override;
    void onCollision(ACore &, ISprite *) override;

    sf::Packet getEntityPacket(network::PACKET_TYPE) override;
    void updateFromPacket(sf::Packet packet) override;
};

namespace NetworkConstructor {
    IEntity *createDumbMonsterFromPacket(ACore &core, sf::Packet packet);
};