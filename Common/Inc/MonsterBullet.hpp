//
// Created by kaww on 27/09/18.
//

#pragma once

#include "IBullet.hpp"

#define MONSTERBULLET_ID 20

class MonsterBullet : public IBullet {
    float _counter;
    float _originalY;
    float _amplitude;
public:
    MonsterBullet(sf::Vector2f &pos, float amplitude = 1, std::string bullet = "./Assets/Sprites/MonsterBullet.png");
    ~MonsterBullet();
    void updateBullet(ACore &Acore) override;
    void onCollision(ACore &, ISprite *) override;

    sf::Packet getEntityPacket(network::PACKET_TYPE) override;
};

namespace NetworkConstructor {
    IEntity *createMonsterBulletFromPacket(ACore &core, sf::Packet packet);
};