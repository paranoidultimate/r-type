//
// Created by kaww on 27/09/18.
//

#pragma once

#include "ISprite.hpp"

#define IMONSTER_ID 19

class IMonster : public ISprite {
protected:
	float _counter;

protected:
	float _originalY;
	float _speed;
    float _scale;
    float _amplitude;
    float _amplitudeSpeed;
	bool _willDropPowerUp;
public:
    IMonster(sf::Vector2f &pos, std::string texturePath, float speed, float amplitude, float amplitudeSpeed, float scale = 1);
	~IMonster();
	void update(ACore &core) override;
	virtual void updateMonster(ACore &core);

	void setOriginalY(float _originalY);
	float getOriginalY() const;

	float getCounter() const;
	void setCounter(float _counter);
};
