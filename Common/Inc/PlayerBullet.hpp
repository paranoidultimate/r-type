//
// Created by reimuanal on 27/09/18.
//

#pragma once

#include "IBullet.hpp"

#define PLAYERBULLET_ID 5

/*
 * PlayerBullet define the implementation of bullet shot by any player.
 */

class PlayerBullet : public IBullet {
    float _counter;
    float _originalY;
    float _amplitude;
public:
    PlayerBullet(sf::Vector2f &pos, float amplitude = 1);
    virtual ~PlayerBullet(){};
    void updateBullet(ACore &core) override;
    void onCollision(ACore &core, ISprite *) override;
    sf::Packet getEntityPacket(network::PACKET_TYPE) override;
};

namespace NetworkConstructor {
    IEntity *createPlayerBulletFromPacket(ACore &core, sf::Packet);
};