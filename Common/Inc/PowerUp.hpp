//
// Created by reimua on 27/11/18.
//

#pragma once

#include "ISprite.hpp"

#define POWERUP_ID 12

class PowerUp : public ISprite {
	float _originalY;
	float _inc;
public:
	PowerUp(sf::Vector2f &pos);
	virtual ~PowerUp();
	void update(ACore &core) override;
	void onCollision(ACore &core, ISprite *ohter) override {};
};

namespace NetworkConstructor {
	IEntity *createPowerUpFromPacket(ACore &core, sf::Packet packet);
}