//
// Created by kaww on 27/09/18.
//

#include "PlayerBullet.hpp"
#include "ActionManager.hpp"
#include "DumbMonster.hpp"

DumbMonster::DumbMonster(sf::Vector2f &pos, float scale) :
IMonster(pos, "./Assets/Sprites/DumbMonster.png", 0.3, 1, 0.01, scale),
_health(10)

{
    _entityType = DUMBMONSTER_ID;
}

DumbMonster::~DumbMonster()
{
}

void DumbMonster::updateMonster(ACore &core) {
    if (_health < 0) {
        core.addToDeletionQueue(this);
        sf::Vector2f dumbPos(100, (std::rand() % 200) - 100);
        auto monster = new DumbMonster(dumbPos, 4);
        core.feedEntity(monster);
    }
}

void DumbMonster::onCollision(ACore &core, ISprite *sprite)
{
    if (sprite->getEntityType() == PLAYERBULLET_ID) {
		_health--;
        core.addToDeletionQueue(sprite);
    }
}

sf::Packet DumbMonster::getEntityPacket(network::PACKET_TYPE packetType)
{
    sf::Packet packet;

    packet << packetType;
    if (packetType == network::ENT_UPDATE) {
        packet << _id;
    }
    packet << DUMBMONSTER_ID << _id;
    packet << _pos.x << _pos.y << _amplitude << _speed << _amplitudeSpeed << _scale << _originalY << _counter;
    return packet;
}

void DumbMonster::updateFromPacket(sf::Packet packet)
{
    int id;
    std::string texturePath;
    float posX;
    float posY;

    packet >> id;
    packet >> posX;
    packet >> posY;
    _pos.x = posX;
    _pos.y = posY;
}

IEntity *NetworkConstructor::createDumbMonsterFromPacket(ACore &core, sf::Packet packet)
{
    int id;
    sf::Vector2f pos;
    float amplitude;
    float speed;
    float amplitudeSpeed;
    float scale;
    float originalY;
    float counter;

    packet >> id >> pos.x >> pos.y >> amplitude >> speed >> amplitudeSpeed >> scale >> originalY >> counter;

    IMonster *entity = new DumbMonster(pos, scale);
    entity->setOriginalY(originalY);
    entity->setCounter(counter);
    entity->setId(id);
    return entity;
}