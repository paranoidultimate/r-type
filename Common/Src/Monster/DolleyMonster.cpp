//
// Created by kaww on 27/09/18.
//

#include "PlayerBullet.hpp"
#include "ActionManager.hpp"
#include "DolleyMonster.hpp"
#include "HealthUp.hpp"

DolleyMonster::DolleyMonster(sf::Vector2f &pos) :
IMonster(pos, "./Assets/Sprites/DolleyMonster.png", 0.3, 3, 0.05, 0.3),
_health(150)

{
    _entityType = DOLLEYMONSTER_ID;
}

DolleyMonster::~DolleyMonster()
{
}

void DolleyMonster::updateMonster(ACore &core) {
    if (_health < 0) {
        core.addToDeletionQueue(this);
        auto healthUp = new HealthUp(_pos);
        core.feedEntity(healthUp);
    }
}

void DolleyMonster::onCollision(ACore &core, ISprite *sprite)
{
    if (sprite->getEntityType() == PLAYERBULLET_ID) {
		_health--;
        core.addToDeletionQueue(sprite);
    }
}

sf::Packet DolleyMonster::getEntityPacket(network::PACKET_TYPE packetType)
{
    sf::Packet packet;

    packet << packetType;
    if (packetType == network::ENT_UPDATE) {
        packet << _id;
    }
    packet << DOLLEYMONSTER_ID << _id;
    packet << _pos.x << _pos.y << _scale << _originalY << _counter;
    return packet;
}

void DolleyMonster::updateFromPacket(sf::Packet packet)
{
    int id;
    std::string texturePath;
    float posX;
    float posY;

    packet >> id;
    packet >> posX;
    packet >> posY;
    _pos.x = posX;
    _pos.y = posY;
}

IEntity *NetworkConstructor::createDolleyMonsterFromPacket(ACore &core, sf::Packet packet)
{
    int id;
    sf::Vector2f pos;
    float scale;
    float originalY;
    float counter;

    packet >> id >> pos.x >> pos.y >> scale >> originalY >> counter;

    IMonster *entity = new DolleyMonster(pos);
    entity->setOriginalY(originalY);
    entity->setCounter(counter);
    entity->setId(id);
    return entity;
}