//
// Created by kaww on 27/09/18.
//

#include "PlayerBullet.hpp"
#include "ActionManager.hpp"
#include "VenerMonster.hpp"

VenerMonster::VenerMonster(sf::Vector2f &pos, float scale) :
IMonster(pos, "./Assets/Sprites/VenerMonster.png", 0.1, 0.2, 0.01, scale),
_health(60)

{
    _entityType = VENERMONSTER_ID;
}

VenerMonster::~VenerMonster()
{
}

void VenerMonster::updateMonster(ACore &core) {
    if (_health < 0) {
        core.addToDeletionQueue(this);
        auto monsterPos = sf::Vector2f(100, 0);
            auto monster = new VenerMonster(monsterPos, 4);
            core.feedEntity(monster);
    }
}

void VenerMonster::onCollision(ACore &core, ISprite *sprite)
{
    if (sprite->getEntityType() == PLAYERBULLET_ID) {
		_health--;
        core.addToDeletionQueue(sprite);
    }
}

sf::Packet VenerMonster::getEntityPacket(network::PACKET_TYPE packetType)
{
    sf::Packet packet;

    packet << packetType;
    if (packetType == network::ENT_UPDATE) {
        packet << _id;
    }
    packet << VENERMONSTER_ID << _id;
    packet << _pos.x << _pos.y << _scale << _originalY << _counter;
    return packet;
}

void VenerMonster::updateFromPacket(sf::Packet packet)
{
    int id;
    std::string texturePath;
    float posX;
    float posY;

    packet >> id;
    packet >> posX;
    packet >> posY;
    _pos.x = posX;
    _pos.y = posY;
}

IEntity *NetworkConstructor::createVenerMonsterFromPacket(ACore &core, sf::Packet packet)
{
    int id;
    sf::Vector2f pos;
    float scale;
    float originalY;
    float counter;

    packet >> id >> pos.x >> pos.y >> scale >> originalY >> counter;

    IMonster *entity = new VenerMonster(pos, scale);
    entity->setOriginalY(originalY);
    entity->setCounter(counter);
    entity->setId(id);
    return entity;
}