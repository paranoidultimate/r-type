//
// Created by reimua on 27/11/18.
//

#include "ISprite.hpp"
#include "BlackStar.hpp"
#include "PlayerBullet.hpp"
#include "PowerUp.hpp"
#include "BlackStarBullet.hpp"
#include "DolleyMonster.hpp"

BlackStar::BlackStar(sf::Vector2f &pos) :
ISprite(pos, "./Assets/Sprites/BlackStar.png", 3, PT_COLLIDER),
_health(800),
_counter(0)
{
	_entityType = BLACKSTAR_ID;
}

BlackStar::~BlackStar()
{
}

void BlackStar::burst(ACore &core)
{
	if (_counter < 20) {
		sf::Vector2f pos(_pos);

		pos.x += rand() % 7 - 7;
		core.feedEntity(new BlackStarBullet(pos));
		core.feedEntity(new BlackStarBullet(pos, -1));
	}
	_counter++;
	if (_counter == 40)
		_counter = 0;
}

void BlackStar::update(ACore &core) {
	if (_health < 0) {
		core.addToDeletionQueue(this);
		sf::Vector2f pos(160, rand() % 100 - 50);
		core.feedEntity(new BlackStar(pos));
		core.feedEntity(new PowerUp(_pos));
		core.feedEntity(new DolleyMonster(_pos));
	}
	if (_pos.x < -130) {
		_health = 1600;
		_pos.x = 130;
	}
	_pos.x -= 0.1;
	this->burst(core);
	core.setOnTop(_id);
}

void BlackStar::onCollision(ACore &core, ISprite *sprite)
{
	if (sprite->getEntityType() == PLAYERBULLET_ID) {
		_health--;
		core.addToDeletionQueue(sprite);
	}
}

void BlackStar::incStreamTimer()
{
	_streamTimer += 15;
}

sf::Packet BlackStar::getEntityPacket(network::PACKET_TYPE packetType)
{
	sf::Packet packet;

	packet << packetType;
	if (packetType == network::ENT_UPDATE)
		packet << _id;
	packet << _entityType << _id << _texturePath << _pos.x << _pos.y;
	if (packetType == network::ENT_UPDATE) {
		packet << _packetNbr++;
	}
	return packet;
}

IEntity *NetworkConstructor::createBlackStarFromPacket(ACore &core, sf::Packet packet)
{
	int id;
	int playerNbr;
	sf::Vector2f pos;
	std::string textPath;

	packet >> id >> textPath >> pos.x >> pos.y;

	ISprite *sprite = new BlackStar(pos);
	sprite->setId(id);
	return sprite;
}