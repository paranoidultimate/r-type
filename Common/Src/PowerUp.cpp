//
// Created by reimua on 27/11/18.
//

#include <cmath>
#include "PowerUp.hpp"

PowerUp::PowerUp(sf::Vector2f &pos) :
ISprite(pos, "./Assets/Sprites/PowerUp.png", 0.1, PT_LAZY),
_originalY(pos.y),
_inc(0)
{
	_entityType = POWERUP_ID;
}

PowerUp::~PowerUp() {}

void PowerUp::update(ACore &core)
{
	_pos.x -= 0.1;
}

IEntity *NetworkConstructor::createPowerUpFromPacket(ACore &core, sf::Packet packet)
{
	int id;
	sf::Vector2f pos;
	std::string textPath;

	packet >> id >> textPath >> pos.x >> pos.y;

	ISprite *sprite = new PowerUp(pos);
	sprite->setId(id);
	return sprite;
}