//
// Created by kaww on 27/11/18.
//

#include <cmath>
#include "HealthUp.hpp"

HealthUp::HealthUp(sf::Vector2f &pos) :
ISprite(pos, "./Assets/Sprites/HealthUp.png", 0.1, PT_LAZY),
_originalY(pos.y),
_inc(0)
{
	_entityType = HEALTHUP_ID;
}

HealthUp::~HealthUp() {}

void HealthUp::update(ACore &core)
{
	_pos.x -= 0.1;
}

IEntity *NetworkConstructor::createHealthUpFromPacket(ACore &core, sf::Packet packet)
{
	int id;
	sf::Vector2f pos;
	std::string textPath;

	packet >> id >> textPath >> pos.x >> pos.y;

	ISprite *sprite = new HealthUp(pos);
	sprite->setId(id);
	return sprite;
}