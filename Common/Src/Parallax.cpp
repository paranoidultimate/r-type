//
// Created by reimua on 26/11/18.
//

#include <Parallax.hpp>

#include "Parallax.hpp"
#include <iostream>

Parallax::Parallax(sf::Vector2f &pos) :
ISprite(pos, "Assets/Sprites/starfieldBlue.png", 1, PT_NONE),
_clock(),
_currentTotal(_clock.elapsedMilliseconds()),
_lastTotal(_clock.elapsedMilliseconds())
{
	_entityType = PARALLAX_ID;
	_clock.tick();
}

Parallax::~Parallax() {}

IEntity *NetworkConstructor::createParallaxFromPacket(ACore &core, sf::Packet packet)
{
	return nullptr;
}

void Parallax::update(ACore &) {
	_currentTotal = _clock.totalMicroseconds();

	if (_currentTotal - _lastTotal >= 1000000 / 60){
		_clock.frame();
		if (_pos.x <= -200)
			_pos.x += 200;
		else
			_pos.x -= 0.5;
		_lastTotal = _currentTotal;
	}
	_clock.tick();
}

void Parallax::updateFromPacket(sf::Packet)
{
}

void Parallax::onCollision(ACore &core, ISprite *other)
{
}

