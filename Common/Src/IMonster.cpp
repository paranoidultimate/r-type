//
// Created by kaww on 27/09/18.
//

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include "IMonster.hpp"
#include "MonsterBullet.hpp"
#include "Clock.hpp"
#include "AudioManager.hpp"
#include "ClassicMonster.hpp"

IMonster::IMonster(sf::Vector2f &pos, std::string texturePath, float speed, float amplitude, float amplitudeSpeed, float scale) :
        ISprite(pos, texturePath, scale, PT_COLLIDER),
        _counter(0),
		_originalY(pos.y),
		_amplitude(amplitude),
		_scale(scale),
		_amplitudeSpeed(amplitudeSpeed)
{
	_entityType = IMONSTER_ID;
	_speed = (speed < 0.1) ? 0.1 : (speed > 1) ? 1 : speed;
	_amplitude = (amplitude < 0.2) ? 0.2 : (amplitude > 10) ? 10 : amplitude;
	_amplitudeSpeed = (amplitudeSpeed < 0.01) ? 0.01 : (amplitudeSpeed > 0.05) ? 0.05 : amplitudeSpeed;

	if ((std::rand() % 100) < 4)
		this->_willDropPowerUp = true;
	else
		this->_willDropPowerUp = false;
}

IMonster::~IMonster()
{
}

void IMonster::update(ACore &core)
{
	_counter += _amplitudeSpeed;

	_pos.x -= _speed;
	_pos.y = _originalY + cos(_counter) * 10 * _amplitude;

	if (std::rand() % 1000 < 5)
	{
		auto bullet = new MonsterBullet(this->_pos);
		core.feedEntity(bullet);
	}

	if (_pos.x < -110 || _pos.x > 110 || _pos.y < -130 || _pos.y > 130) {
		_pos.x = 109;
		_pos.y = _originalY;
	}
	this->updateMonster(core);
}

void IMonster::updateMonster(ACore &core)
{
	_counter += _amplitudeSpeed;

    _pos.x -= _speed;
    _pos.y = _originalY + cos(_counter) * 10 * _amplitude;

    if (std::rand() % 100 < 2)
    {
    	auto bullet = new MonsterBullet(this->_pos);
    	core.feedEntity(bullet);
    	core.getAudioManager()->playSound("./Assets/Audio/PlayerLaser.ogg");
    }
}

float IMonster::getOriginalY() const {
    return _originalY;
}

void IMonster::setOriginalY(float _originalY) {
	IMonster::_originalY = _originalY;
}

float IMonster::getCounter() const {
	return _counter;
}

void IMonster::setCounter(float _counter) {
	IMonster::_counter = _counter;
}











