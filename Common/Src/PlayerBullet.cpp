//
// Created by reimuanal on 27/09/18.
//

#include <cmath>
#include <NetworkManager.hpp>
#include "ActionManager.hpp"
#include "PlayerBullet.hpp"
#include "ClassicMonster.hpp"

PlayerBullet::PlayerBullet(sf::Vector2f &pos, float amplitude) :
IBullet(pos, "./Assets/Sprites/PlayerBullet.png"),
_counter(3.14 / 2),
_originalY(pos.y),
_amplitude(amplitude)
{
    _entityType = PLAYERBULLET_ID;
}

void PlayerBullet::updateBullet(ACore &core)
{
    _counter += 0.2;

    _pos.x += 4;
    _pos.y = _originalY + cos(_counter) * 10 * _amplitude;
}

void PlayerBullet::onCollision(ACore &core, ISprite *sprite)
{
}

IEntity *NetworkConstructor::createPlayerBulletFromPacket(ACore &core, sf::Packet packet)
{
    int id;
    sf::Vector2f pos;
    float amplitude;

    packet >> id >> pos.x >> pos.y >> amplitude;
    IEntity *entity = new PlayerBullet(pos, amplitude);
    entity->setId(id);
    return entity;
}


sf::Packet PlayerBullet::getEntityPacket(network::PACKET_TYPE packetType)
{
    sf::Packet packet;

    packet << packetType;
    if (packetType == network::ENT_UPDATE)
        packet << _id;
    packet << PLAYERBULLET_ID << _id << _pos.x << _pos.y << _amplitude;
    return packet;
}