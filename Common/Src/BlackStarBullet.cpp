//
// Created by reimua on 27/11/18.
//

#include "BlackStarBullet.hpp"

BlackStarBullet::BlackStarBullet(sf::Vector2f &pos, float amplitude, int count) :
		IBullet(pos, "./Assets/Sprites/BlackStarBullet.png"),
		_counter(count),
		_originalY(pos.y),
		_amplitude(amplitude)
{
	_entityType = BLACKSTARBULLET_ID;
}

void BlackStarBullet::updateBullet(ACore &core)
{
	_pos.y += 2.5 * _amplitude;
}

void BlackStarBullet::onCollision(ACore &core, ISprite *sprite)
{
}

IEntity *NetworkConstructor::createBlackStarBulletFromPacket(ACore &core, sf::Packet packet)
{
	int id;
	sf::Vector2f pos;
	float amplitude;
	int tmp;

	packet >> id >> pos.x >> pos.y >> amplitude >> tmp;
	IEntity *entity = new BlackStarBullet(pos, amplitude, tmp);
	entity->setId(id);
	return entity;
}


sf::Packet BlackStarBullet::getEntityPacket(network::PACKET_TYPE packetType)
{
	sf::Packet packet;

	packet << packetType;
	if (packetType == network::ENT_UPDATE)
		packet << _id;
	packet << _entityType << _id << _pos.x << _pos.y << _amplitude << _counter;
	return packet;
}