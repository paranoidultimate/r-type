//
// Created by reimuanal on 27/09/18.
//

#include "IBullet.hpp"
#include "AudioManager.hpp"

IBullet::IBullet(sf::Vector2f &pos, std::string texturePath) :
        ISprite(pos, texturePath, 1, PT_LAZY)
{
    _entityType = IBULLET_ID;
}

IBullet::~IBullet()
{
}

void IBullet::update(ACore &core)
{
    if (_pos.x < -110 || _pos.x > 110 || _pos.y < -130 || _pos.y > 130) {
        core.addToDeletionQueue(this);
    }
    this->updateBullet(core);
}

void IBullet::incStreamTimer()
{
}