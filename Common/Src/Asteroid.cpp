//
// Created by reimua on 27/11/18.
//

#include <cmath>
#include "ISprite.hpp"
#include "Asteroid.hpp"
#include "PlayerBullet.hpp"
#include "PowerUp.hpp"

Asteroid::Asteroid(sf::Vector2f &pos, int spriteNbr) :
		ISprite(pos, "./Assets/Sprites/Asteroid" + std::to_string(spriteNbr) + ".png", 1.5, PT_COLLIDER),
		_health(30),
		_spriteNbr(spriteNbr),
		_originalPos(pos),
		_target(-90, rand() % 150 - 75)
{
	_entityType = ASTEROID_ID;
}

Asteroid::~Asteroid()
{
}

void Asteroid::update(ACore &core) {
	_pos.x -= 1;
	double deltaY = sqrt(_originalPos.y * _originalPos.y) + sqrt(_target.y * _target.y);
	double deltaX = sqrt(_originalPos.x * _originalPos.x) + sqrt(_target.x * _target.x);
	_pos.y += (deltaY / deltaX);

	if (_pos.x < -110) {
		_pos.x = 110;
		_pos.y = rand() % 50 - 100;
	}
	if (_health < 0) {
		_pos.x = 110;
		_pos.y = rand() % 50 - 100;
		core.feedEntity(new Asteroid(_pos, rand() % 7 + 1));
		core.addToDeletionQueue(this);
	}
}

void Asteroid::onCollision(ACore &core, ISprite *sprite)
{
	if (sprite->getEntityType() == PLAYERBULLET_ID) {
		_health--;
		core.addToDeletionQueue(sprite);
	}
}

void Asteroid::incStreamTimer()
{
	_streamTimer += 15;
}

sf::Packet Asteroid::getEntityPacket(network::PACKET_TYPE packetType)
{
	sf::Packet packet;

	packet << packetType;
	if (packetType == network::ENT_UPDATE)
		packet << _id;
	packet << _entityType << _id << _texturePath << _pos.x << _pos.y << _spriteNbr;
	if (packetType == network::ENT_UPDATE) {
		packet << _packetNbr++;
	}
	return packet;
}

IEntity *NetworkConstructor::createAsteroidFromPacket(ACore &core, sf::Packet packet)
{
	int id;
	int spriteNbr;
	sf::Vector2f pos;
	std::string textPath;

	packet >> id >> textPath >> pos.x >> pos.y >> spriteNbr;

	ISprite *sprite = new Asteroid(pos, spriteNbr);
	sprite->setId(id);
	return sprite;
}