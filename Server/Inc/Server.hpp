//
// Created by reimua on 26/11/18.
//

#pragma once

#include <SFML/Network.hpp>
#include "ACore.hpp"

namespace Server {
	void start(unsigned short);
	void entityFeeder(ACore &core);
	void threadEntryPoint(int portNbr);
	void onPlayerJoin(ACore&, sf::IpAddress sender, unsigned short senderPort);
};