//
// Created by reimua on 26/11/18.
//

#include <ctime>
#include <thread>
#include <SFML/System.hpp>
#include <SFML/Network.hpp>
#include <iostream>
#include <Asteroid.hpp>

#include "Core.hpp"
#include "Server.hpp"
#include "Player.hpp"
#include "BlackStar.hpp"
#include "NetworkManager.hpp"

#include "ClassicMonster.hpp"
#include "DumbMonster.hpp"
#include "SpeedMonster.hpp"
#include "VenerMonster.hpp"
#include "DolleyMonster.hpp"
#include <string.h>

void Server::onPlayerJoin(ACore &core, sf::IpAddress sender, unsigned short senderPort)
{
	sf::Vector2f pos(-90, 0);
	core.feedEntity(new Player(pos, (int)core.getNetworkManager()->getClientVector().size()));
	for (const auto &value: core.getNetworkManager()->getClientVector()) {
		if (value.ip == sender && value.port == senderPort)
			return;
	}
	client_s client;

	for (int i = 0; i < sf::Keyboard::KeyCount; i++)
		client._keyMap[i] = 0;
	client.ip = sender;
	client.port = senderPort;
	core.getNetworkManager()->getClientVector().push_back(client);
}

void Server::threadEntryPoint(int portNbr)
{
	sf::Vector2i screenSize(1920, 720);
	Core core(screenSize);
	auto networkManager = core.getNetworkManager();

	core.setServerStatus(true);
	networkManager->setPlayerJoinCallback(Server::onPlayerJoin);
	networkManager->bindSocket(portNbr);
	Server::entityFeeder(core);
	core.run();
}

void Server::entityFeeder(ACore &core)
{

	sf::Vector2f pos(100, 0);
	auto monsterPos = sf::Vector2f(100, 0);
	for (int i = 0; i < 2; i++)
	{
		sf::Vector2f randomPos(100, (std::rand() % 200) - 100);
		float speed = (float)(std::rand() % 200) / 100;
		float amplitude = (float)(std::rand() % 1000) / 100;
		float amplitudeSpeed = (float)(std::rand() % 5) / 100;
		auto monster = new ClassicMonster(monsterPos,
										  speed,
										  amplitude,
										  amplitudeSpeed, 4);

		auto dumbMonster = new DumbMonster(randomPos, 4);
		auto speedMonster = new SpeedMonster(randomPos, 4);
		auto venerMonster = new VenerMonster(randomPos, 4);
		core.feedEntity(monster);
		core.feedEntity(dumbMonster);
		core.feedEntity(speedMonster);
		core.feedEntity(venerMonster);
	}

	sf::Vector2f v(90, 0);
	core.feedEntity(new BlackStar(v));
	sf::Vector2f pos2(100, 0);
	core.feedEntity(new Asteroid(pos2));
}

void Server::start(unsigned short port)
{
	int idx = 0;
	sf::Packet packet;
	sf::UdpSocket socket;
	sf::IpAddress sender;
	unsigned short senderPort;
	std::vector<sf::Thread *> threads;

	socket.bind(port);
	while (true) {
		auto state = socket.receive(packet, sender, senderPort);
		if (state == sf::Socket::Done) {
			if (idx % 4 == 0) {
				port++;
				auto *thread = new sf::Thread(Server::threadEntryPoint, port);
				threads.push_back(thread);
				thread->launch();
			}
			auto *answer = new sf::Packet();
			*answer << network::PORT_REDIRECTION << port;
			socket.send(*answer, sender, senderPort);
			delete answer;
			idx++;
		}
	}
}