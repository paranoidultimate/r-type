LINUX/MAC

How to compile :

run command 'mkdir build && cd build && cmake .. && make'

How to run :

for server run './r-type_server {PORT NUMBER}'
for client run './r-type_client {IP ADDRESS} {PORT NUMBER}'


WINDOWS in Powershell

How to compile :

run command 'mkdir build ; cd build ; cmake .. ; cmake --build . --target r-type_server ; cmake --build . --target r-type_client'

How to run :

for server run '& "./r-type_server.exe" {PORT NUMBER}'
for client run '& "./r-type_client.exe" {IP ADDRESS} {PORT NUMBER}'