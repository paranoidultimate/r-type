/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <assert.h>
#include <stdexcept>
#include <criterion/criterion.h>
#include "ISprite.hpp"
#include "PacketType.hpp"

Test(Sprite, getPacketType)
{
	sf::Vector2f pos(10, 10);

	ISprite sprite(pos, "lommdr");

	sf::Packet packet = sprite.getEntityPacket(network::ENT_UPDATE);

	int i;
	packet >> i;
	assert(i == network::ENT_UPDATE);
}

Test(Sprite, getPacketTypeS)
{
sf::Vector pos(10, 10);

ISprite sprite(pos, "lommdr");

sf::Packet packet = sprite.getEntityPacket(network::ENT_CREATION);

int i;
packet >> i;
assert(i == network::ENT_UPDATE);
}