/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <string>
#include <vector>
#include <criterion/criterion.h>
#include "StringHelper.hpp"

Test(StringHelper, trim)
{
	std::string str(" Yeee boi	     i'm a string #with a comment");
	std::string trimmed("Yeee boi      i'm a string");
	
	StringHelper::trimString(str);
	cr_assert_eq(str, trimmed);
}

Test(StringHelper, trimEmpty)
{
	std::string str("   		# ");
	std::string str2("");
	std::string trimmed("");
	
	StringHelper::trimString(str);
	StringHelper::trimString(str2);
	cr_assert_eq(str, trimmed);
	cr_assert_eq(str2, trimmed);
}

Test(StringHelper, reduce)
{
	std::string str("Yeee boi      i'm a string");
	std::string reduced("Yeee boi i'm a string");

	StringHelper::reduceString(str);
	cr_assert_eq(str, reduced);
}

Test(StringHelper, reduceEmpty)
{
	std::string str("      ");
	std::string reduced("");

	StringHelper::reduceString(str);
	cr_assert_eq(str, reduced);
}

Test(StringHelper, epureString)
{
	std::string str(" lol 	xd qdl	# 		");
	std::string epured("lol xd qdl");

	StringHelper::epureString(str);
	cr_assert_eq(str, epured);
}

Test(StringHelper, epureStringVector)
{
	std::vector<std::string> vector;
	std::vector<std::string> epured;
	vector.push_back(" lol ");
	vector.push_back(" m  d 		r ");
	vector.push_back(" uno petit  smilo :k	xd");
	epured.push_back("lol");
	epured.push_back("m d r");
	epured.push_back("uno petit smilo :k xd");
	StringHelper::epureStringVector(vector);
	for (unsigned int i = 0; i < vector.size(); i++)
		cr_assert_eq(vector[i], epured[i]);
}

Test(StringHelper, explodeString)
{
	std::string str("Allez    on va  pété toussa x d");
	std::vector<std::string> vector =
				StringHelper::explodeString(str, ' ');
	cr_assert_eq(vector[0], "Allez");
	cr_assert_eq(vector[1], "on");
	cr_assert_eq(vector[2], "va");
	cr_assert_eq(vector[3], "pété");
	cr_assert_eq(vector[4], "toussa");
	cr_assert_eq(vector[5], "x");
	cr_assert_eq(vector[6], "d");
	cr_assert_eq(vector.size(), 7);
}

Test(StringHelper, explodeStringEmpty)
{
	std::string str("");
	std::vector<std::string> vector =
				StringHelper::explodeString(str, ' ');
	cr_assert_eq(vector[0], "");
	cr_assert_eq(vector.size(), 1);
}