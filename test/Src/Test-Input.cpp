/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "Input.hpp"

Test(Input, computeAndSetValue)
{
	Input _input;

	cr_assert(_input.compute(1) == (-true));
	_input.setValue(1);
	cr_assert(_input.compute(1) == true);
	_input.setValue(0);
	cr_assert(_input.compute(1) == false);
}

Test(Input, computeError)
{
	Input _input;

	try {
		_input.compute(2);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}

Test(Input, getPinType)
{
	Input _input;

	cr_assert(_input.getPinType(1) == nts::TRANSMITTER);
}

Test(Input, allSetLinkCase)
{
	Input _input;

	try {
		_input.setLink(2, _input, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_input.setLink(1, _input, 1);
	try {
		_input.setLink(1, _input, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}