/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "C4069.hpp"
#include "Input.hpp"

Test(C4069, computeUndefinedPin)
{
	C4069 _c4069;

	for (int i = 0; i < 14; i++)
		cr_assert_eq(_c4069.compute(i + 1), (-true));
}

Test(C4069, getPinType)
{
	C4069 _c4069;
	nts::PinType type;

	for (int i = 1; i < 15; i++) {
		if (i == 7 || i == 14)
			type = nts::NONE;
		else if (i % 2)
			type = nts::RECEIVER;
		else
			type = nts::TRANSMITTER;
		cr_assert_eq(_c4069.getPinType(i), type);
	}
}

Test(C4069, allSetLinkCase)
{
	C4069 _c4069;

	try {
		_c4069.setLink(15, _c4069, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_c4069.setLink(1, _c4069, 1);
	try {
		_c4069.setLink(1, _c4069, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}

Test(C4069, computeAllCaseGate1)
{
	Input i1;
	C4069 _c4069;

	i1.setLink(1, _c4069, 1);
	_c4069.setLink(1, i1, 1);
	cr_assert_eq(_c4069.compute(2), (-true));
	i1.setValue(0);
	cr_assert_eq(_c4069.compute(2), true);
	i1.setValue(1);
	cr_assert_eq(_c4069.compute(2), false);
}

Test(C4069, computeAllCaseGate2)
{
	Input i1;
	C4069 _c4069;

	i1.setLink(1, _c4069, 3);
	_c4069.setLink(3, i1, 1);
	cr_assert_eq(_c4069.compute(4), (-true));
	i1.setValue(0);
	cr_assert_eq(_c4069.compute(4), true);
	i1.setValue(1);
	cr_assert_eq(_c4069.compute(4), false);
}

Test(C4069, computeAllCaseGate3)
{
	Input i1;
	C4069 _c4069;

	i1.setLink(1, _c4069, 5);
	_c4069.setLink(5, i1, 1);
	cr_assert_eq(_c4069.compute(6), (-true));
	i1.setValue(0);
	cr_assert_eq(_c4069.compute(6), true);
	i1.setValue(1);
	cr_assert_eq(_c4069.compute(6), false);
}

Test(C4069, computeAllCaseGate4)
{
	Input i1;
	C4069 _c4069;

	i1.setLink(1, _c4069, 9);
	_c4069.setLink(9, i1, 1);
	cr_assert_eq(_c4069.compute(8), (-true));
	i1.setValue(0);
	cr_assert_eq(_c4069.compute(8), true);
	i1.setValue(1);
	cr_assert_eq(_c4069.compute(8), false);
}

Test(C4069, computeAllCaseGate5)
{
	Input i1;
	C4069 _c4069;

	i1.setLink(1, _c4069, 11);
	_c4069.setLink(11, i1, 1);
	cr_assert_eq(_c4069.compute(10), (-true));
	i1.setValue(0);
	cr_assert_eq(_c4069.compute(10), true);
	i1.setValue(1);
	cr_assert_eq(_c4069.compute(10), false);
}

Test(C4069, computeAllCaseGate6)
{
	Input i1;
	C4069 _c4069;

	i1.setLink(1, _c4069, 13);
	_c4069.setLink(13, i1, 1);
	cr_assert_eq(_c4069.compute(12), (-true));
	i1.setValue(0);
	cr_assert_eq(_c4069.compute(12), true);
	i1.setValue(1);
	cr_assert_eq(_c4069.compute(12), false);
}