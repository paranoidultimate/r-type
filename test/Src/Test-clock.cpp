/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <assert.h>
#include <criterion/criterion.h>
#include "Clock.hpp"

Test(Clock, init_ellapsed)
{
    Clock clock;

    assert(clock.elapsedMicroseconds() == 0);
    assert(clock.elapsedMilliseconds() == 0);
    assert(clock.elapsedSeconds() == 0);
}

Test(Clock, init_Total)
{
    Clock clock;

    assert(clock.totalMicroseconds() == 0);
    assert(clock.totalMilliseconds() == 0);
    assert(clock.totalSeconds() == 0);
}

Test(Clock, frame)
{
    Clock clock;

    clock.tick();
    clock.frame();
    assert(clock.elapsedMicroseconds() == 0);
    assert(clock.elapsedMilliseconds() == 0);
    assert(clock.elapsedSeconds() == 0);
}

Test(Clock, tick)
{
    Clock clock;

    clock.tick();
    assert(clock.elapsedMicroseconds() == 0);
}
