/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "Input.hpp"
#include "Output.hpp"

Test(Output, compute)
{
	Output _output;

	cr_assert(_output.compute(1) == (-true));
}

Test(Output, computeWithInput)
{
	Output _output;
	Input _input;

	_input.setLink(1, _output, 1);
	_output.setLink(1, _input, 1);
	cr_assert(_output.compute(1) == (-true));
	_input.setValue(0);
	cr_assert(_output.compute(1) == false);
	_input.setValue(1);
	cr_assert(_output.compute(1) == true);
}

Test(Output, computeError)
{
	Output _output;

	try {
		_output.compute(2);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}

Test(Output, getPinType)
{
	Output _output;

	cr_assert(_output.getPinType(1) == nts::RECEIVER);
}

Test(Output, allSetLinkCase)
{
	Output _output;

	try {
		_output.setLink(2, _output, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_output.setLink(1, _output, 1);
	try {
		_output.setLink(1, _output, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}