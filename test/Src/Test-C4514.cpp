/*
** EPITECH PROJECT, 2018
** singe
** File description:
** singe
*/

#include <stdexcept>
#include <criterion/criterion.h>
#include "C4514.hpp"
#include "Input.hpp"

Test(C4514, computeUndefinedPin)
{
	C4514 _c4514;

	for (int i = 0; i < 24; i++)
		cr_assert_eq(_c4514.compute(i + 1), (-true));
}

Test(C4514, getPinType)
{
	C4514 _c4514;
	nts::PinType type;

	for (int i = 1; i < 25; i++) {
		if ((i <= 3) || (i >= 21 && i <= 23))
			type = nts::RECEIVER;
		else if (i == 24 || i == 12)
			type = nts::NONE;
		else
			type = nts::TRANSMITTER;
		cr_assert_eq(_c4514.getPinType(i), type);
	}
}

Test(C4514, allSetLinkCase)
{
	C4514 _c4514;

	try {
		_c4514.setLink(25, _c4514, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
	_c4514.setLink(1, _c4514, 1);
	try {
		_c4514.setLink(1, _c4514, 1);
		cr_assert(0);
	} catch (const std::exception &e) {
		(void)e;
	}
}