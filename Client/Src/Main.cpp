//
// Created by reimua on 19/11/18.
//

#include <string>
#include <stdio.h>
#include <unistd.h>
#include <BlackStar.hpp>
#include <BlackStarBullet.hpp>
#include <PowerUp.hpp>
#include <HealthUp.hpp>
#include <Asteroid.hpp>

#include "Core.hpp"
#include "Parallax.hpp"
#include "Player.hpp"
#include "PlayerBullet.hpp"

#include "MonsterBullet.hpp"
#include "PacketType.hpp"
#include "ClassicMonster.hpp"
#include "DumbMonster.hpp"
#include "SpeedMonster.hpp"
#include "VenerMonster.hpp"
#include "DolleyMonster.hpp"

int main(int argc, char **argv)
{
    if (argc != 3)
        return 84;
    std::string ip = std::string(argv[1]);
    unsigned short port = atoi(argv[2]);
    sf::Vector2i screenSize(1920, 720);
    Core core(screenSize);
    sf::Vector2f pos(0, 0);
    sf::Packet packet;
    auto parallax = new Parallax(pos);
    parallax->setId(-1);
    core.feedEntity(parallax);
	core.setServerStatus(false);
	packet << network::PLAYER_JOIN;
	auto networkManager = new NetworkManager();
    auto actionManager = core.getActionManager();
    core.getAudioManager()->playBgm("./Assets/Audio/BackgroundMusic.ogg");
    networkManager->addNetworkConstructor(PLAYER_ID, NetworkConstructor::createPlayerFromPacket);
    networkManager->addNetworkConstructor(PLAYERBULLET_ID, NetworkConstructor::createPlayerBulletFromPacket);
    networkManager->addNetworkConstructor(MONSTERBULLET_ID, NetworkConstructor::createMonsterBulletFromPacket);
	networkManager->addNetworkConstructor(CLASSICMONSTER_ID, NetworkConstructor::createClassicMonsterFromPacket);
	networkManager->addNetworkConstructor(PARALLAX_ID, NetworkConstructor::createParallaxFromPacket);
	networkManager->addNetworkConstructor(BLACKSTAR_ID, NetworkConstructor::createBlackStarFromPacket);
	networkManager->addNetworkConstructor(BLACKSTARBULLET_ID, NetworkConstructor::createBlackStarBulletFromPacket);
	networkManager->addNetworkConstructor(DOLLEYMONSTER_ID, NetworkConstructor::createDolleyMonsterFromPacket);
    networkManager->addNetworkConstructor(DUMBMONSTER_ID, NetworkConstructor::createDumbMonsterFromPacket);
    networkManager->addNetworkConstructor(SPEEDMONSTER_ID, NetworkConstructor::createSpeedMonsterFromPacket);
    networkManager->addNetworkConstructor(VENERMONSTER_ID, NetworkConstructor::createVenerMonsterFromPacket);
	networkManager->addNetworkConstructor(POWERUP_ID, NetworkConstructor::createPowerUpFromPacket);
    networkManager->addNetworkConstructor(HEALTHUP_ID, NetworkConstructor::createHealthUpFromPacket);
	networkManager->addNetworkConstructor(ASTEROID_ID, NetworkConstructor::createAsteroidFromPacket);
    networkManager->_ipTarget = ip;
    networkManager->_portTarget = port;
    networkManager->sendPacket(packet, ip, port);
    sleep(1);

    core.setNetworkManager(networkManager);

    core.run();
    return 0;
}
