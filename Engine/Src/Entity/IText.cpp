/*
** EPITECH PROJECT, 2018
** LOL
** File description:
** singe
*/

#include "NetworkManager.hpp"
#include "IText.hpp"

IText::IText(sf::Vector2f &pos, std::string &content) :
IEntity(),
_font(),
_pos(pos),
_text()
{
    _entityType = ITEXT_ID;
	_font.loadFromFile("./Assets/arial.ttf");
	_text.setFont(_font);
	_text.setString(content);
	_text.setPosition(pos);
}

void IText::setString(std::string &content)
{
	_text.setString(content);
}

void IText::render(ACore &core)
{
    sf::Vector2f textPos;

    textPos.x = core.absoluteToRelativeX(_pos.x);
    textPos.y = core.absoluteToRelativeY(_pos.y);

    _text.setPosition(textPos);
	if (core.getRenderWindow() != nullptr)
		core.getRenderWindow()->draw(_text);
}

void IText::update(ACore &core)
{

}

sf::Packet IText::getEntityPacket(network::PACKET_TYPE packetType)
{
	sf::Packet packet;

	packet << packetType << _id << _text.getString() << _pos.x << _pos.y;
	return packet;
}