/*
** EPITECH PROJECT, 2018
**  Oooh Oooh
** File description:
**    Singe
*/

#include <SFML/Graphics.hpp>
#include "ActionManager.hpp"

ActionManager::ActionManager() :
_focus(true),
_previousState(),
_currentState(),
_internalKeyMap()
{
    mapKey(sf::Keyboard::Up);
    mapKey(sf::Keyboard::Left);
    mapKey(sf::Keyboard::Down);
    mapKey(sf::Keyboard::Right);
    mapKey(sf::Keyboard::W);
}

ActionManager::~ActionManager()
{
}

void ActionManager::mapKey(sf::Keyboard::Key key)
{
	_previousState[key] = _currentState[key] = false;
}

bool ActionManager::isKeyMapped(sf::Keyboard::Key key)
{
	if (_previousState.find(key) == _previousState.end() || _currentState.find(key) == _currentState.end())
		return false;
	return true;
}

bool ActionManager::isKeyPressed(sf::Keyboard::Key key)
{
	if (!this->isKeyMapped(key))
		this->mapKey(key);
	return (!_previousState[key] && _currentState[key]);
}

bool ActionManager::isKeyReleased(sf::Keyboard::Key key)
{
	if (!this->isKeyMapped(key))
		this->mapKey(key);
	return (_previousState[key] && !_currentState[key]);
}

bool ActionManager::isKeyDown(sf::Keyboard::Key key)
{
	if (!this->isKeyMapped(key))
		this->mapKey(key);
	return _currentState[key];
}

bool ActionManager::isKeyUp(sf::Keyboard::Key key)
{
	if (!this->isKeyMapped(key))
		this->mapKey(key);
	return !_currentState[key];
}

void ActionManager::flush()
{
    if (!_focus)
        return;
	for (auto it = _currentState.begin(); it != _currentState.end(); ++it)
	{
		_previousState[it->first] = _currentState[it->first];
		_currentState[it->first] = sf::Keyboard::isKeyPressed(it->first);
		if (_currentState[it->first])
			_internalKeyMap[it->first] = 1;
	}
}

std::vector<sf::Keyboard::Key> ActionManager::getPressedKey()
{
    std::vector<sf::Keyboard::Key> pressedKey;

    for (auto it = _currentState.begin(); it != _currentState.end(); ++it) {
        if (this->isKeyPressed(it->first) || this->isKeyDown(it->first))
            pressedKey.push_back(it->first);
    }
    return pressedKey;
}

void ActionManager::setFocus(bool focus)
{
	_focus = focus;
}