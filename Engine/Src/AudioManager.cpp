/*
** EPITECH PROJECT, 2018
**  <------------>
** File description:
**    <------->
*/

#include "AudioManager.hpp"

AudioManager::AudioManager() : _playing(false), _bgm()
{
}

AudioManager::~AudioManager()
{
}

void AudioManager::stopBgm()
{
	if (_playing == true) {
		_bgm.stop();
		_playing = false;
	}
}

bool AudioManager::playBgm(const std::string name)
{
	if (_playing == true) {
		_bgm.stop();
		_playing = false;
	}
	if (_bgm.openFromFile(name) == true) {
		_bgm.play();
		_playing = true;
		return true;
	}
	return false;
}

bool AudioManager::playSound(const std::string name)
{
	sf::Music sound;

	sound.openFromFile(name);
	sound.play();
	return true;
}
