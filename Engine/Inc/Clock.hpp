/*
** EPITECH PROJECT, 2018
**  <------------>
** File description:
**    <------->
*/

#pragma once

#include <cstddef>
#include <SFML/System/Clock.hpp>

class Clock final {
public:
	Clock();
	~Clock();

	void tick();
	void frame();

	size_t elapsedMicroseconds() const;
	size_t elapsedMilliseconds() const;
	size_t elapsedSeconds() const;

	size_t totalMicroseconds() const;
	size_t totalMilliseconds() const;
	size_t totalSeconds() const;

private:
	sf::Clock _clock;
	std::size_t _currentTime;
	std::size_t _lastTime;
	std::size_t _elapsedTime;
	std::size_t _totalTime;
};
