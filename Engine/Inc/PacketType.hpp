//
// Created by reimua on 21/11/18.
//

#pragma once

namespace network {
    enum PACKET_TYPE {
        PORT_REDIRECTION,
    	PLAYER_JOIN,
        ENT_CREATION,
        ENT_UPDATE,
        ENT_DESTRUCTION,
        STREAM_END,
        INPUT
    };
};