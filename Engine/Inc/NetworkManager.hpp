//
// Created by reimua on 19/11/18.
//

#pragma once

#include <vector>
#include <string>
#include <functional>

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include "IEntity.hpp"

#include "ACore.hpp"
#include "ActionManager.hpp"
#include "PacketType.hpp"
#include "INetworkManager.hpp"

class NetworkManager final : public INetworkManager {
    // Engine data
    std::map<int, std::function<IEntity *(ACore &core, sf::Packet)>> _constructor;
    void (*_playerJoinCallback)(ACore&, sf::IpAddress sender, unsigned short senderPort);
public:
	// Socket data
    sf::UdpSocket _socket;
	std::vector<client_s> _clients;
	std::string _ipTarget;
    unsigned short _portTarget;
    bool _binded;

    NetworkManager();
    ~NetworkManager();

    void bindSocket(unsigned short port) override;
    void streamEntityUpdate(IEntity *) override;
    void streamEntityCreation(IEntity *) override;
    void streamEntityDestruction(IEntity *) override;
    void streamInput(IActionManager *actionManager) override;

    void endStream() override;

    void sendPacket(sf::Packet packet, sf::IpAddress recipient, unsigned short port) override;

    void readSocket(ACore &core) override;

    bool isClientKeyPressed(int clientNbr, sf::Keyboard::Key key) override;
    void addNetworkConstructor(int objectID, IEntity *(constructor)(ACore&, sf::Packet)) override;
	void setPlayerJoinCallback(void (*callback)(ACore&, sf::IpAddress sender, unsigned short senderPort)) override;

	std::vector<client_s> &getClientVector() override;
};