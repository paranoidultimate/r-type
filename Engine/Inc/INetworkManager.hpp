//
// Created by reimua on 25/11/18.
//

#pragma once

//
// Created by reimua on 19/11/18.
//

#pragma once

#include <SFML/Network.hpp>
#include "IEntity.hpp"

#include "ACore.hpp"
#include "IActionManager.hpp"
#include "PacketType.hpp"

struct client_s {
	sf::IpAddress ip;
	unsigned short port;
	int _keyMap[sf::Keyboard::KeyCount];
};

class INetworkManager {
public:
	INetworkManager(){};
	virtual ~INetworkManager(){};

	virtual void bindSocket(unsigned short port) = 0;

	virtual void streamEntityUpdate(IEntity *) = 0;
	virtual void streamEntityCreation(IEntity *) = 0;
	virtual void streamEntityDestruction(IEntity *) = 0;
	virtual void streamInput(IActionManager *actionManager) = 0;

	virtual void endStream() = 0;

	virtual void sendPacket(sf::Packet packet, sf::IpAddress recipient, unsigned short port) = 0;
	virtual void readSocket(ACore &core) = 0;
	virtual bool isClientKeyPressed(int clientNbr, sf::Keyboard::Key key) = 0;
	virtual void addNetworkConstructor(int objectID, IEntity *(constructor)(ACore &, sf::Packet)) = 0;
	virtual void setPlayerJoinCallback(void (*playerJoinCallBack)(ACore&, sf::IpAddress sender, unsigned short senderPort)) = 0;

	virtual std::vector<client_s> &getClientVector() = 0;
};