/*
** EPITECH PROJECT, 2018
** LOL
** File description:
** singe
*/

#pragma once

#include <string>

#include "IEntity.hpp"
#include "SFML/Graphics.hpp"

#define ITEXT_ID 2

class IText : public IEntity {
private:
	sf::Font _font;
protected:
	sf::Vector2f _pos;
	sf::Text _text;
public:
	IText(sf::Vector2f &pos, std::string &content);
	virtual ~IText(){};

	void setString(std::string &str);
	void render(ACore &core) override;
    void update(ACore &core) override;


	sf::Packet getEntityPacket(network::PACKET_TYPE) override;
};