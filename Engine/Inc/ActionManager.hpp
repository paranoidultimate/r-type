/*
** EPITECH PROJECT, 2018
**  <------------>
** File description:
**    <------->
*/

#pragma once

#include "IActionManager.hpp"

#include <map>
#include <SFML/Graphics.hpp>

class ActionManager final : public IActionManager {
public:
	ActionManager();
	~ActionManager();

	bool isKeyPressed(sf::Keyboard::Key key) override;
	bool isKeyReleased(sf::Keyboard::Key key) override;
	bool isKeyDown(sf::Keyboard::Key key) override;
	bool isKeyUp(sf::Keyboard::Key key) override;
	bool isKeyMapped(sf::Keyboard::Key key) override;
    std::vector<sf::Keyboard::Key> getPressedKey() override;
	void flush() override;

	void setFocus(bool focus) override;
private:
	void mapKey(sf::Keyboard::Key key);

	bool _focus;
	int _internalKeyMap[sf::Keyboard::KeyCount];
	std::map<sf::Keyboard::Key, bool> _currentState;
	std::map<sf::Keyboard::Key, bool> _previousState;
};
