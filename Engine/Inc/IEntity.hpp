/*
** EPITECH PROJECT, 2018
** LOL
** File description:
** singe
*/

#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>

#include "ACore.hpp"
#include "PacketType.hpp"

#define IENTITY_ID 0

/*
 * SET MANUALLY THE ENTITY TYPE OF THE CLASS VIA THE SETTER IN YOUR CLASS CONSTRUCTOR
 */

/*
 * IEntity define the core element in the game engine.
 */

class IEntity {
protected:
	int _id;
	int _entityType;
	int _streamTimer;
public:
	static int makeId();
	// Constructor
	IEntity();
	virtual ~IEntity(){};

	// gameEngine related method
	virtual void update(ACore &core) = 0;
	virtual void render(ACore &core) = 0;
	int getId() const;
	void setId(int);

	void setEntityType(int type);
	int getEntityType() const;

	// Network related method

	virtual void incStreamTimer();
	virtual void resetStreamTimer();
	virtual int getStreamTimer();

    virtual sf::Packet getEntityPacket(network::PACKET_TYPE) = 0;
    virtual void updateFromPacket(sf::Packet) = 0;
};
