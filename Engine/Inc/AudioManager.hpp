/*
** EPITECH PROJECT, 2018
**  <------------>
** File description:
**    <------->
*/

#pragma once

#include <string>
#include <SFML/Audio.hpp>
#include "IAudioManager.hpp"

class AudioManager final : public IAudioManager {
private:
	bool _playing;
	sf::Music _bgm;
public:
	AudioManager();
	~AudioManager();
	void stopBgm() override;
	bool playBgm(const std::string name) override;
	bool playSound(const std::string name) override;
};

/*
 *  The ServerAudioManager is an empty sound manager, so no sound is played for the server.
 */

class ServerAudioManager final : public IAudioManager {
public:
	ServerAudioManager(){};
	~ServerAudioManager(){};
	void stopBgm() override {};
	bool playBgm(const std::string name) override {return false; };
	bool playSound(const std::string name) override { return false; };
};
