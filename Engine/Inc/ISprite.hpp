/*
** EPITECH PROJECT, 2018
** LOL
** File description:
** singe
*/

#pragma once

#include <string>
#include <SFML/Graphics.hpp>
#include "IEntity.hpp"

enum physicType {
	PT_NONE,
	PT_LAZY,
	PT_COLLIDER
};

#define ISPRITE_ID 1

class ISprite : public IEntity {
protected:
	std::string _texturePath;
	sf::Sprite 	_sprite;
	sf::Vector2f _pos;
	sf::Texture	_texture;
	enum physicType _physicType;
	unsigned int _packetNbr;
public:
	ISprite(sf::Vector2f &pos, std::string texturePath, float scale = 1, enum physicType = PT_NONE);

	virtual ~ISprite(){};

	sf::Sprite getSprite() const;
	void setSprite(sf::Sprite);
	enum physicType getPhysicType() const;

	virtual void onCollision(ACore &, ISprite *) = 0; // this function is useless if PT_NONE is specified.
	virtual sf::Packet getEntityPacket(network::PACKET_TYPE) override;

	virtual void updateFromPacket(sf::Packet) override;

	void render(ACore &core) override;
};
